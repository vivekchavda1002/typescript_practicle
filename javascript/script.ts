/* appendig the results in input box */
const concatResult = (inputValue:String):void=>{
        let elementResult:HTMLInputElement = document.getElementById("result") as HTMLInputElement;
        if(elementResult.value != null){
            elementResult.value += inputValue;
        }        
}
/* setting values of input*/
const setResult = (inputValue:string):void=>{
    let elementResult:HTMLInputElement = document.getElementById("result") as HTMLInputElement;
    elementResult.value = inputValue;    
}
/* get the values of input*/
const getResult = ():string=>{
    let elementResult:HTMLInputElement = document.getElementById("result") as HTMLInputElement;
    let result:string =  elementResult.value;
    return result;
}
/* Evaluting the expression */
const equal=():void=>{
    try{
        setResult(eval(getResult()))
    }catch(err){
        setResult("Error")
    }
}
/* erase results in  */
const erase=(opration:string):void => {
    if(opration == "clear"){
        let result:string = getResult();
        setResult(result.substring(0,result.length-1))
    }else if(opration == "allClr"){
        setResult("")
    }
}
/* const to find log and In oprations */
const log = (val:string):void => {
    let value:number = +getResult();
    if (val.match('log')) {
        setResult(Math.log10(value).toString());
    } else if (val.match('in')) {
        setResult(Math.log(value).toString());
    }
}

const squareRoot=():void=> {
    let result:number = +getResult();
    setResult(Math.pow(result, 2).toString())
}
const pie = ():void =>{
    let result:number =+getResult();
    setResult(eval(`${result} * 3.14159265359`));
}
const factorial=(n:number):number =>{
    if(n <= 0){
        return 1
    }else{
        return n * factorial(n-1);
    }
}
const fact = () :void =>{
    let result:number = +getResult();
    setResult(factorial(result).toString());
}
const reciprocal = ():void =>{
    let result:number = +getResult();
    setResult(eval(`1/${result}`));
}
const twoRootX = ():void =>{
    let result = getResult();
    setResult(eval(`2*Math.sqrt(${result})`));
}
const tenX = () =>{
    let result:number = +getResult();
    setResult(eval(`10 ** ${result}`));
}
const negativeNumber = (val:string) =>{
    let result:number = +getResult();
    if (val.match('mod')) {
            setResult(eval(`${result} * (-1)`)) 
    } else if (val.match('neg')) {
        setResult(eval(`${result} * (-1)`))
    }
}
/* All trigo consts*/
const trigonometry = (val:string)=> {
    let result:number = +getResult();
    if (val.match('sin')) {
        setResult(Math.sin(result).toString())
    } else if (val.match('cos')) {
        setResult(Math.cos(result).toString())
    } else if (val.match('tan')) {
        setResult(Math.tan(result).toString())
    }
}
/* All Aggregate consts */
const aggregate = (val:string) =>{
    let result:number = +getResult();
    if (val.match('acos')) {
        setResult(Math.acos(result).toString())
    } else if (val.match('abs')) {
        setResult(Math.abs(result).toString())
    } else if (val.match('acosh')) {
       setResult(Math.acosh(result).toString())
    } else if (val.match('arg')) {
        setResult(Math.abs(result).toString())
    } else if (val.match('asin')) {
        setResult(Math.asin(result).toString())
    }
}
/* memory oprations */
const memory = ():object =>{
    let m:number = 0;
    return {
        mPlus: () => m = eval(`${m} + ${getResult()}`),
        mMinus: () => m = eval(`${m} - ${getResult()}`),
        mStore: () => m = +getResult(),
        mClear: () => m = 0,
        mRecall: () => setResult(m.toString())
    }
}
let memoryOpration:object = memory();
/* const for Scientific Exponent */
const fe = ():void =>{
    let result = getResult();
    setResult(Number.parseFloat(result).toExponential(2))
}
/* const for Euler e Opration */
const expo = ()=> {
    let result:number = +getResult();
    setResult(Math.exp(result).toString())
}
